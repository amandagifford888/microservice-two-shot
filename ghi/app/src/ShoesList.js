import { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';
import React from 'react';

function ShoesList(props) {
    const [shoes, setShoes] = useState([]);
    const [hasDeleted, setHasDeleted] = useState(false);


    const deleteShoes = async (shoeId) => {
        console.log(shoeId);
        try {
            const response = await fetch(`http://localhost:8080/api/delete/${shoeId}/shoes/`, {
                method: 'DELETE'
            });
            console.log(response);
            if (response.ok) {
                setShoes((prevShoes) => prevShoes.filter((shoe) => shoe.id !== shoeId));
                setHasDeleted(true);
                window.location.reload();
            } else {
                console.error("Not able to delete shoes.");
            }
        }
        catch (error) {
            console.error('error', error);
        }
    }
    let messageClasses = 'alert alert-success d-none mb-0';
    let tableClasses = 'table table-striped table-hover';
    if (hasDeleted) {
        messageClasses = 'alert alert-success mb-0';
        tableClasses = "table table-striped table-hover d-none";
    }

    return (
        <>
        <table className={tableClasses}>
          <thead>
            <tr>
              <th>Model Name</th>
              <th>Manufacturer</th>
              <th>Bin</th>
              <th>Color</th>
              <th>Image</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {props.shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.model_name }</td>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.bin.closet_name }</td>
                  <td>{ shoe.color }</td>
                  <td><img src={ shoe.imageUrl } className="thumbnail" width="12%"/></td>
                  <td>
                    <button onClick={() => deleteShoes(shoe.id)}>Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {hasDeleted && (
            <div className={messageClasses} id="success-message">
            The shoes have been deleted!
        </div>
        )}
    </>

    );
  }
    // const [shoes, setShoes] = useState([]);

    // useEffect(() => {
    //     const fetchShoes = async () => {
    //         const response = await fetch('http://localhost:8080/api/shoes/');
    //         const data = await response.json();
    //         setShoes(data);
    //         console.log(data, shoes)
    //     };

    //     fetchShoes();
    // }, []);

    // const deleteShoe = (id) => async () => {
    //     // there has got to be a better way to do this
    //     try {
    //         // send delete request
    //         const response = await fetch(`http://localhost:8080/api/detail/${id}/shoes/`, {
    //             method: 'DELETE',
    //           });
    //         if (!response.ok) {
    //             console.error("Failure to delete:::::Response issue")
    //         } else {
    //             // set state to be everything but deleted Shoe... this seems wrong
    //             setShoes(shoes.filter(shoe => shoe.id !== id));
    //         }

    //     } catch (error) {
    //         // sigh
    //         console.error("Another fail", error)
    //     }
    // }

//     return (
//         <div>
//             {
//             props.shoes.map(shoe => {
//                 <p key={shoe.href}>{shoe.name}</p>
//             }

//             )}
//         </div>
//     );
// }

export default ShoesList;

// return (
//     <p key={shoe.href}>

//     </p>
// )})}

// <Link to={`/Shoes/${shoe.id}`}>{shoe.name}</Link>
// <button onClick={deleteShoe(shoe.id)}>Button</button>

import React, {useEffect, useState} from 'react';

function ShoesForm() {
    const [bins, setBins] = useState([]);
    const [bin, setBin] = useState("");
    const [modelName, setModelName] = useState("");
    const [manufacturer, setManufacturer] = useState("");
    const [color, setColor] = useState("");
    const [imageUrl, setImageUrl] = useState("");

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/";

        const response = await fetch(url);
        console.log(response);
        if (response.ok) {
            const data = await response.json();

            setBins(data.bins);
        }
    }

    useEffect ( () => {
        fetchData();
    }, []);

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleImageUrlChange = (event) => {
        const value = event.target.value;
        setImageUrl(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.bin = bin;
        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.color = color;
        data.image_url = imageUrl;

        console.log(data);

        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setBin('');
            setModelName('');
            setManufacturer('');
            setColor('');
            setImageUrl('');
        }
    }


    return (
    <div className="my-5 container">
    <div className="row">
        <div className="col col-sm-auto">
        </div>
        <div className="col">
        <div className="card shadow">
            <div className="card-body">
            <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Shoes time!</h1>
                <p className="mb-3">
                Make some shoes
                </p>
                <div id="loading-conference-spinner">
                <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
                </div>
                <div className="mb-3">
                </div>
                <p className="mb-3">
                Now, tell us about yourself.
                </p>
                <div className="row">
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleModelNameChange} required placeholder="Model Name" type="text" id="model_name" name="model_name" value={modelName} className="form-control"/>
                    <label htmlFor="modelName">Model Name</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} required placeholder="manufacturer" type="text" id="manufacturer" name="manufacturer" value={manufacturer} className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleColorChange} required placeholder="color" type="text" id="color" name="color" value={color} className="form-control"/>
                    <label htmlFor="color">Color</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleImageUrlChange}required placeholder="image_url" type="text" id="image_url" name="image_url" value={imageUrl} className="form-control"/>
                    <label htmlFor="image_url">Image Url</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <select onChange={handleBinChange} name="bin" id="bin" className="form-select">
                    <option value="">Choose a bin</option>
                        {
                            bins.map(bin => {
                                return (
                                    <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                                )
                            })
                        }
                    </select>
                    </div>
                </div>
                </div>
                <button className="btn btn-lg btn-primary">Shoes!</button>
            </form>
            <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all organized'
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>);
}

export default ShoesForm;

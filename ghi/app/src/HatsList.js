import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function HatsList() {
    const [hats, setHats] = useState([]);

    useEffect(() => {
        const fetchHats = async () => {
            const response = await fetch('http://localhost:8090/api/hats/');
            const data = await response.json();
            setHats(data.CoolHats);
            // console.log(data, hats)
        };

        fetchHats();
    }, []);

    const deleteHat = (id) => async () => {
        // there has got to be a better way to do this
        // console.log('trying to delete this hat with id:', id);
        try {
            // send delete request
            const response = await fetch(`http://localhost:8090/api/detail/${id}/hats/`, {
                method: 'DELETE',
              });
            if (!response.ok) {
                console.error("Failure to delete:::::Response issue")
            } else {
                // set state to be everything but deleted hat... this seems wrong
                setHats(hats.filter(hat => hat.id !== id));
            }

        } catch (error) {
            // sigh
            console.error("Another fail", error)
        }
    }

    return (
        <div>
            {
            hats.map(hat => {
                return (
                    <p key={hat.name}>
                        <Link to={`/hats/${hat.id}`}>{hat.name}</Link>
                        <button onClick={deleteHat(hat.id)}>Delete this very cool hat with this very ugly button</button>
                    </p>
                )})}
        </div>
    );
}

export default HatsList;

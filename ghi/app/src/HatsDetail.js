import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";

function HatsDetail(props) {
    const [hat, setHat] = useState({});
    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        const fetchHats = async () => {
            try{
                const response = await fetch(`http://localhost:8090/api/detail/${id}/hats/`);
                // console.log(response)
                const data = await response.json();
                setHat(data);
                // console.log(data, hat)
            } catch (error) {
                console.error("You fucked up somewhere: ", error, "fix it:)");
                // this is good practice this is good practice
            }
        };

        fetchHats();
    }, [id]);

    async function deleteHat() {
        try {
            const response = await fetch(`http://localhost:8090/api/detail/${hat.id}/hats/`, {
                method: 'DELETE',
              });
            if (!response.ok) {
                console.error("Failure to delete:::::Response issue")
            } else {
                navigate("/hats");
            }
        } catch (error) {
            console.error("Another fail", error)
        }
    }

    return (
        <div>
            {/* check to see if hat isnt null... no idea why i need to do it twice, or even once, but it works, good practice with the ? anyways */}
            {hat && hat.name ? (
                <>
                    <h1>{hat.name}</h1>
                    <img src={hat.pictureUrl} alt="I didnt link an actual url to an image because i was lazy" />
                    <p>Made from the plushest {hat.fabric}.  Pineapple is a wonderful topping{hat.is_pineapple_top_three_pizza_toppings}.  Crafted with love in the style of {hat.style}, its color is {hat.color}.</p>
                    <p>location: {hat.location.closet_name}</p>
                    <button onClick={deleteHat}>Delete this very cool hat with this very ugly button</button>
                </>
            ) : (
                <p>Why doesnt this work!!!!!!!!!!!</p>
            )}
        </div>
    );
}

export default HatsDetail

import React, {useEffect, useState} from 'react';

function HatsForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState("");
    const [fabric, setFabric] = useState("");
    const [color, setColor] = useState("");
    const [style, setStyle] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [location, setLocation] = useState("");

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);
        // console.log(response);
        if (response.ok) {
            const data = await response.json();

            setLocations(data.locations);
        }
    }

    useEffect ( () => {
        fetchData();
    }, []);

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.fabric = fabric;
        data.style = style;
        data.color = color;
        data.pictureUrl = pictureUrl;
        data.location = location;

        // console.log(data);

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            // console.log(newHat);

            setLocation('');
            setName('');
            setFabric("");
            setStyle("");
            setColor('');
            setPictureUrl('');
        }
    }


    return (
    <div className="my-5 container">
    <div className="row">
        <div className="col col-sm-auto">
        </div>
        <div className="col">
        <div className="card shadow">
            <div className="card-body">
            <form onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">Hats and Forms</h1>
                <p className="mb-3">
                Birth a Hat
                </p>
                <div id="loading-conference-spinner">
                <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
                </div>
                <div className="mb-3">
                </div>
                <p className="mb-3">
                What would you like?
                </p>
                <div className="row">
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleNameChange} required placeholder="Model Name" type="text" id="name" name="name" value={name} className="form-control"/>
                    <label htmlFor="modelName">Model Name</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleFabricChange} required placeholder="fabric" type="text" id="fabric" name="fabric" value={fabric} className="form-control"/>
                    <label htmlFor="Fabric">Fabric</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleStyleChange} required placeholder="style" type="text" id="style" name="style" value={style} className="form-control"/>
                    <label htmlFor="style">style</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleColorChange} required placeholder="color" type="text" id="color" name="color" value={color} className="form-control"/>
                    <label htmlFor="color">Color</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handlePictureChange}required placeholder="picture URL please" type="url" id="pictureUrl" name="pictureUrl" value={pictureUrl} className="form-control"/>
                    <label htmlFor="image_url">Image Url</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <select onChange={handleLocationChange} name="location" id="location" className="form-select">
                    <option value="">Choose your location wisley</option>
                        {
                            locations.map(location => {
                                return (
                                    <option key={location.href} value={location.href}>{location.closet_name}</option>
                                )
                            })
                        }
                    </select>
                    </div>
                </div>
                </div>
                <button className="btn btn-lg btn-primary">Magic!</button>
            </form>
            <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all organized'
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>);
}

export default HatsForm;

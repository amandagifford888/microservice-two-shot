import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesForm from "./ShoesForm";
import HatsForm from './HatsForm';
import HatsList from './HatsList';
import HatsDetail from './HatsDetail';
import ShoesList from './ShoesList';

function App(props) {
  // useState is a React Hook that allows you to add state to a functional component. It returns an array with
  // two values: the current state and a function to update it.

  const [locations, setLocations] = useState([]);
  const [bins, setBins] = useState([]);
  const [hats, setHats] = useState([]);
  const [shoes, setShoes] = useState([]);

  // getLocations function makes a fetch call using the fetch() method to start the process of fetching a resource from
  // a server. It returns a Promise that resolves to a Response object. If the response is valid and complete, use .json()
  // method to parse the response into json and set that equal to the variable data. Now you can set the state of the locations
  // current state variable locations from useState hook.
  //
  async function getLocations() {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  async function getBins() {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  async function loadHats() {
    const response = await fetch("http://localhost:8090/api/hats/");
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    } else {
      console.error(response);
    }
  }

  async function loadShoes() {
    const response = await fetch("http://localhost:8080/api/shoes/");
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    } else {
      console.error(response);
    }
  }

  // useEffect allows you to perform side effects in functional components
  useEffect(() => {
    getLocations();
    getBins();
    loadHats();
    loadShoes();
  }, []);




  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage/>} />
          <Route path="shoes">
            <Route index element={<ShoesList shoes={shoes} />} />
            <Route path="new" element={<ShoesForm getBins={getBins} bins={bins} />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatsList hats={hats} />} />
            <Route path="new" element={<HatsForm getLocations={getLocations} locations={locations} />} />
            <Route path=":id" element={<HatsDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

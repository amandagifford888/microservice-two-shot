# Generated by Django 4.0.3 on 2023-06-02 16:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LocationVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('closet_name', models.CharField(max_length=420)),
                ('section_number', models.PositiveSmallIntegerField()),
                ('shelf_number', models.PositiveSmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='MigrationModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=420)),
            ],
        ),
        migrations.CreateModel(
            name='CoolHats',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=420)),
                ('is_pizza_in_the_top_three_best_pizza_toppings', models.BooleanField(default=True, editable=False)),
                ('fabric', models.CharField(max_length=420)),
                ('style', models.CharField(max_length=420)),
                ('color', models.CharField(max_length=420)),
                ('pictureUrl', models.URLField(max_length=420)),
                ('location', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='Pugs_are_ugly_dogs_and_they_cant_breathe', to='hats_rest.locationvo')),
            ],
        ),
    ]

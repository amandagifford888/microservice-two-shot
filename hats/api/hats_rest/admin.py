from django.contrib import admin
from .models import LocationVO, MigrationModel, CoolHats


@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'closet_name',
        'section_number', 'shelf_number',
        'import_href'
    )


@admin.register(MigrationModel)
class MigrationModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


@admin.register(CoolHats)
class CoolHatsAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'is_pineapple_top_three_pizza_toppings',
        'fabric', 'style', 'color', 'pictureUrl', 'location'
        )

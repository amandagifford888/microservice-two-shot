from django.db import models


class MigrationModel(models.Model):
    name = models.CharField(max_length=420)
    def __str__(self):
        return self.name


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=420)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=420)


    def __str__(self):
        return self.closet_name


class CoolHats(models.Model):
    name = models.CharField(max_length=420)
    id = models.AutoField(primary_key=True)
    is_pineapple_top_three_pizza_toppings = models.BooleanField(
        default=True,
        editable=False,
        null=True
        )
    fabric = models.CharField(max_length=420)
    style = models.CharField(max_length=420)
    color = models.CharField(max_length=420)
    pictureUrl = models.URLField(max_length=420, null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="Pugs_are_ugly_dogs_and_they_cant_breathe",
        on_delete=models.PROTECT,
    )


    def __str__(self):
        return self.name

""" Create your models here.  The Hat resource should track its fabric,
its style name, its color, a URL for a picture,
and the location in the wardrobe where it exists.
class Location(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    def get_api_url(self):
        return reverse("api_location", kwargs={"pk": self.pk})
    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"
    class Meta:
        ordering = ("closet_name", "section_number", "shelf_number")
 """

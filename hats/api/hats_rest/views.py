# from django.shortcuts import render (Why do i need this?)
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from .models import CoolHats, LocationVO
from django.db import IntegrityError
import json
from common.json import ModelEncoder


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
    ]


class HatsListEncoder(ModelEncoder):
    model = CoolHats
    properties = [
        "name",
        "id",
        "is_pineapple_top_three_pizza_toppings",
        "pictureUrl",
        "location"
    ]
    encoders = {"location": LocationVODetailEncoder()}


class HatsDetailEncoder(ModelEncoder):
    model = CoolHats
    properties = [
        "name",
        "id",
        "is_pineapple_top_three_pizza_toppings",
        "pictureUrl",
        "fabric",
        "style",
        "color",
        "location"
    ]
    encoders = {"location": LocationVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hats(request, id=None):
    if request.method == "GET":
        hats = CoolHats.objects.all()
        return JsonResponse(
            {"CoolHats": hats},
            encoder=HatsListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        # print(content)

        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
            # print(content)
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"Message": "Work harder, no sun, crank that neck... also bad location data"},
                status=400,
            )
        hats = CoolHats.objects.create(**content)
        # print(hats)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_hats(request, id):
    try:
        hat = CoolHats.objects.get(id=id)
    except CoolHats.DoesNotExist:
        # print("Danger:::::::::::::::")
        response = JsonResponse({"Heres your tip": "its probably not there"})
        response.status_code = 404

    if request.method == "GET":
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )

    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            response = JsonResponse({"pointer": "JSON smells south of blue cheese"})
            response.status_code = 400

        if "name" in content:
            hat.name = content["name"]
        if "pictureUrl" in content:
            hat.pictureUrl = content["pictureUrl"]
        if "fabric" in content:
            hat.fabric = content["fabric"]
        if "style" in content:
            hat.style = content["style"]
        if "color" in content:
            hat.color = content["color"]
        if "location" in content:

            try:
                location = LocationVO.objects.get(import_href=content["location"]["import_href"])

                hat.location = location

            except LocationVO.DoesNotExist:

                return JsonResponse({"ya gone done fucked up again": "location is fracked"}),

        hat.save()
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        try:
            hat.delete()
            return JsonResponse(
                {"Attention": "Why did you delete that cool hat?"},
            )
        except CoolHats.DoesNotExist:
            return JsonResponse(
                {"Message": "Youre not trying hard enough, this hat is an unknown unknown and does not exist."}
            )

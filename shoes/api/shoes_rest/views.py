from django.shortcuts import render
from .models import Shoes, BinVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["model_name",
                  "bin",
                  "manufacturer",
                  "color",
                  "image_url",
                  "id"]

    encoders = {"bin": BinVODetailEncoder()}


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "bin",
        "manufacturer",
        "color",
        "image_url",
        "id"
    ]

    encoders = {"bin": BinVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        print(shoes)
        return JsonResponse(
            {"shoes": shoes}, encoder=ShoesListEncoder, safe=False)
    else:
        content = json.loads(request.body)
        print(content)
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin id"},
                status=400,
            )

        shoes = Shoes.objects.create(**content)
        print(shoes)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT", "GET"])
def api_detail_shoes(request, pk):
    if request.method == "GET":
        try:
            shoes = Shoes.objects.get(id=pk)
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoesDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            shoes = Shoes.objects.get(id=pk)
        #Get the BinVO id and put it in the content dictionary
            content["bin"] = BinVO.objects.get(import_href=content["bin"])
            props = ["model_name", "manufacturer", "color", "image_url", "bin"]
            for prop in props:
                if prop in content:
                    setattr(shoes, prop, content[prop])
            shoes.save()
            return JsonResponse(shoes, encoder=ShoesDetailEncoder, safe=False)
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Shoes do not exist"})
            response.status_code = 404
            return response
    else:
        try:
            shoes = Shoes.objects.get(id=pk)
            print(shoes)
            shoes.delete()
            return JsonResponse(
                {"message": "The shoes were deleted"},
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Shoes do not exist."})

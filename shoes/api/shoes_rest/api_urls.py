from django.urls import path

from .views import api_list_shoes, api_detail_shoes

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path(
        "bins/<int:bin_vo_id>/shoes/",
        api_list_shoes,
        name="api_list_shoes",
    ),
    path(
        "delete/<int:pk>/shoes/",
        api_detail_shoes,
        name="api_delete_shoes",
    ),
    path("edit/<int:pk>/shoes/", api_detail_shoes, name="api_edit_shoes"),
    path("detail/<int:pk>/shoes/", api_detail_shoes, name="api_detail_shoes"),
]

from django.db import models

"""

    Create your models here. The Shoe resource should track its manufacturer, its model name,
    its color, a URL for a picture, and the bin in the wardrobe where it exists.

#  """
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200)

    def __str__(self):
        return self.closet_name

class Shoes(models.Model):
    model_name = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    image_url = models.URLField(max_length=200)
    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.model_name
